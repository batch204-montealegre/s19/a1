function userInfo(){

	let userName = prompt ("What is your username?");
	let password = prompt(" Enter your password")
	let role = prompt ("What is your role");

	if((userName == null) || ( userName == '')) {
		alert("Username input should not be empty");
	}

	if((password == null) || ( password == '')) {
		alert("Password input should not be empty");
	}

	if((role == null) || ( role == '')) {
		alert("Role input should not be empty");
	}

	else {
		switch(role) {
			case "admin":
				alert("Welcome to the class portal Admin!");
				console.log ("Welcome to the class portal Admin!" + " " +userName);
				break;
			case "teacher":
				alert("Welcome to the class portal Teacher!");
				console.log ("Welcome to the class portal Teacher!" + " " +userName);
				break;
			case "student":
				alert("Welcome to the class portal!" + " " +userName);
				console.log ("Welcome to the class portal!" + " " +userName);
				break;
			default:
				alert("Not valid Role: \n" + " Role should be: admin, student, teacher");
				break;
		}
	}
}

userInfo();


function checkAverage(grade1, grade2, grade3, grade4) {
	let Average = (grade1 + grade2 + grade3 + grade4) / 4;
	Average = Math.round(Average);

	if(Average <= 74) {
		console.log("Hello! Your Average is" + " " +Average + "." + " "+ "Your grade is F");
	} else if ((Average >= 74) && (Average <= 79)) {
		console.log("Hello! Your Average is" + " "  +Average + "." + " "+ "Your grade is D");
	} else if ((Average >= 80) && (Average <= 84)) {
		console.log("Hello! Your Average is" + " "  +Average + "." + " "+ "Your grade is C");
	} else if ((Average >= 85) && (Average <= 89)) {
		console.log("Hello! Your Average is" + " "  +Average + "." + " "+ "Your grade is B");
	} else if ((Average >= 90) && (Average <= 95)) {
		console.log("Hello! Your Average is" + " " +Average + "." + " "+ "Your grade is A");

	} else if (Average >= 96) {
		console.log("Hello! Your Average is" +Average + "." + " "+ "Your grade is A+");
	    
	}
}

console.log('checkAverage(70, 70, 72, 71)');
checkAverage(70, 73, 72, 71);

console.log('checkAverage(76, 76, 77, 79)');
checkAverage(75, 76, 77, 79);

console.log('checkAverage(81, 83, 84, 85)');
checkAverage(80, 81, 82, 78);

console.log('checkAverage(84, 85, 87, 88)');
checkAverage(84, 85, 87, 88);

console.log('checkAverage(89, 90, 91, 90)');
checkAverage(89, 90, 91, 90);

console.log('checkAverage(91, 96, 97, 98)');
checkAverage(91, 96, 97, 98);

console.log('checkAverage(92, 95, 97, 99)');
checkAverage(92, 95, 97, 99);